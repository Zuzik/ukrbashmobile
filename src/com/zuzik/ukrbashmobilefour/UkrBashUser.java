package com.zuzik.ukrbashmobilefour;


public class UkrBashUser {

	public static final String XML_NAME = "name";
	
	private String mName;
	
	public UkrBashUser(){
		mName = "";
	}
	
	public String getName() {
		return mName;
	}
	public void setName(String name) {
		if(name!=null)
			mName = name;
	}
	
}
