package com.zuzik.ukrbashmobilefour;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class DownloadQuotesFragment extends ListFragment {

	protected String WEB_API_METHOD;
	protected QuotesAdapter mAdapter;
	protected ArrayList<UkrBashQuote> mQuotes;
	private volatile boolean isDownload=false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mQuotes = new ArrayList<UkrBashQuote>();
		setListAdapter(createCustomAdapter());
		setRetainInstance(true);
		setHasOptionsMenu(true);
		startDownload();
	}
	
	ListAdapter createCustomAdapter(){
		mAdapter = new QuotesAdapter(mQuotes);
		return mAdapter;
	}
	
	void startDownload(){
		if(!isDownload){
			isDownload=true;
			new FetchItemsTask().execute();
		}
	}
	
	public String getWebApiMethod(){
		return WEB_API_METHOD;
	}
	
	public void setWebApiMethod(String webResources){
		this.WEB_API_METHOD = webResources;
	}
	
	protected class QuotesAdapter extends ArrayAdapter<UkrBashQuote>{
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView==null){
				convertView = getActivity().getLayoutInflater().inflate(R.layout.ukrbash_quote, parent, false);
			}
			
			UkrBashQuote q = getItem(position);
			
			TextView text = (TextView)convertView.findViewById(R.id.quote_id);
			text.setText(q.getId());
			text = (TextView)convertView.findViewById(R.id.quote_rating);
			text.setText(q.getRating());
			text = (TextView)convertView.findViewById(R.id.quote_text);
			text.setText(q.getText());
			text = (TextView)convertView.findViewById(R.id.quote_author);
			text.setText(q.getAuthor());
			text = (TextView)convertView.findViewById(R.id.quote_pub_date);
			text.setText(q.getPubDate());
			
			if(position==getCount()-1){
				startDownload();
			}
			
			return convertView;
		}

		public QuotesAdapter(ArrayList<UkrBashQuote> quotes){
			super(getActivity(), 0, quotes);
		}

	}
	
	private class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<UkrBashQuote>>{
		@Override
		protected ArrayList<UkrBashQuote> doInBackground(Void... params) {
			return new UkrBashFetchr().fetchQuoteItems(getWebApiMethod(), mQuotes.size());
		}
		
		@Override
		protected void onPostExecute(ArrayList<UkrBashQuote> result) {
			mQuotes.addAll(result);
			mAdapter.notifyDataSetChanged();
			isDownload=false;
		}
		
	}
	
}
