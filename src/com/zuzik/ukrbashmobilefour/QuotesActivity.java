package com.zuzik.ukrbashmobilefour;

import android.content.res.Resources;
import android.support.v4.app.Fragment;

public class QuotesActivity extends BaseSlidingActivity {

	private final int COUNT = 4;
	
	@Override
	protected String getTitleByNumber(int number) {
		Resources res = getResources();
		switch (number) {
		case 0:
			return res.getString(R.string.published);
		case 1:
			return res.getString(R.string.unpublished);
		case 2:
			return res.getString(R.string.best);
		case 3:
			return res.getString(R.string.random);
		default:
			return "";
		}
	}

	@Override
	protected Fragment getFragmentByNumber(int number) {
		DownloadQuotesFragment f=null;
		switch (number) {
		case 0:
			f =  new DownloadQuotesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_PUBLISHED_QUOTES);
			return f;
		case 1:
			f =  new DownloadQuotesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_UNPUBLISHED_QUOTES);
			return f;
		case 2:
			f =  new DownloadQuotesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_BEST_QUOTES);
			return f;
		case 3:
			f =  new DownloadQuotesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_RANDOM_QUOTES);
			return f;
		default:
			return null;
		}
	}

	@Override
	protected int getCountOfPages() {
		return COUNT;
	}

   
}
