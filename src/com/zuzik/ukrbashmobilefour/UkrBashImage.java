package com.zuzik.ukrbashmobilefour;

import java.util.Date;

public class UkrBashImage {

	public static final String XML_ID = "id";
	public static final String XML_RATING = "rating";
	public static final String XML_TITLE = "title";
	public static final String XML_IMAGE = "image";
	public static final String XML_THUMBNAIL = "thumbnail";
	public static final String XML_AUTHOR = "author";
	public static final String XML_PUB_DATE = "pub_date";
	
	private String mId;
	private String mRating;
	private String mTitle;
	private String mImage;
	private String mThumbnail;
	private String mAuthor;
	private String mPubDate;
	
	public UkrBashImage(){
		mId="";
		mRating="";
		mTitle="";
		mImage="";
		mThumbnail="";
		mAuthor="";
		mPubDate="";
	}
	
	public String getId() {
		return mId;
	}
	public void setId(String id) {
		if(id!=null)
			mId = id;
	}
	public String getRating() {
		return mRating;
	}
	public void setRating(String rating) {
		if(rating!=null)
			mRating = rating;
	}
	public String getTitle() {
		return mTitle;
	}
	public void setTitle(String text) {
		if(text!=null)
			mTitle = text;
	}
	public String getImage() {
		return mImage;
	}
	public void setImage(String image) {
		if(image!=null)
			mImage = image;
	}
	public String getThumbnail() {
		return mThumbnail;
	}
	public void setThumbnail(String thumbnail) {
		if(thumbnail!=null)
			mThumbnail = thumbnail;
	}
	public String getAuthor() {
		return mAuthor;
	}
	public void setAuthor(String author) {
		if(author!=null)
			mAuthor = author;
	}
	public String getPubDate() {
		return mPubDate;
	}
	public void setPubDate(String pubDate) {
		if(pubDate!=null){
			long timeStamp = Long.parseLong(pubDate);
			Date unixDate = new Date(timeStamp*1000);
			mPubDate = android.text.format.DateFormat.format("dd-MM-yyyy", unixDate).toString();
		}
	}
	
	
	
}
