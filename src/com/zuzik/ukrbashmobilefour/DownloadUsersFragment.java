package com.zuzik.ukrbashmobilefour;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class DownloadUsersFragment extends ListFragment {

	protected String WEB_API_METHOD;
	protected UsersAdapter mAdapter;
	protected ArrayList<UkrBashUser> mUsers;
	private volatile boolean isDownload=false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mUsers = new ArrayList<UkrBashUser>();
		setListAdapter(createCustomAdapter());
		setRetainInstance(true);
		setHasOptionsMenu(true);
		startDownload();
	}
	
	ListAdapter createCustomAdapter(){
		mAdapter = new UsersAdapter(mUsers);
		return mAdapter;
	}
	
	void startDownload(){
		if(!isDownload){
			isDownload=true;
			new FetchItemsTask().execute();
		}
	}
	
	public String getWebApiMethod(){
		return WEB_API_METHOD;
	}
	
	public void setWebApiMethod(String webResources){
		this.WEB_API_METHOD = webResources;
	}
	
	protected class UsersAdapter extends ArrayAdapter<UkrBashUser>{
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView==null){
				convertView = getActivity().getLayoutInflater().inflate(R.layout.ukrbash_user, parent, false);
			}
			
			UkrBashUser q = getItem(position);
			
			((TextView)convertView).setText(q.getName());
			
			if(position==getCount()-1){
				startDownload();
			}
			
			return convertView;
		}

		public UsersAdapter(ArrayList<UkrBashUser> users){
			super(getActivity(), 0, users);
		}

	}
	
	private class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<UkrBashUser>>{
		@Override
		protected ArrayList<UkrBashUser> doInBackground(Void... params) {
			return new UkrBashFetchr().fetchUserItems(getWebApiMethod(), mUsers.size());
		}
		
		@Override
		protected void onPostExecute(ArrayList<UkrBashUser> result) {
			mUsers.addAll(result);
			mAdapter.notifyDataSetChanged();
			isDownload=false;
		}
		
	}
	
}
