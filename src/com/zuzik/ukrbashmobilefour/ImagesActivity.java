package com.zuzik.ukrbashmobilefour;

import android.content.res.Resources;
import android.support.v4.app.Fragment;

public class ImagesActivity extends BaseSlidingActivity {

	private final int COUNT = 4;
	
	@Override
	protected String getTitleByNumber(int number) {
		Resources res = getResources();
		switch (number) {
		case 0:
			return res.getString(R.string.published);
		case 1:
			return res.getString(R.string.unpublished);
		case 2:
			return res.getString(R.string.best);
		case 3:
			return res.getString(R.string.random);
		default:
			return "";
		}
	}

	@Override
	protected Fragment getFragmentByNumber(int number) {
		DownloadImagesFragment f=null;
		switch (number) {
		case 0:
			f =  new DownloadImagesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_PUBLISHED_PICTURES);
			return f;
		case 1:
			f =  new DownloadImagesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_UNPUBLISHED_PICTURES);
			return f;
		case 2:
			f =  new DownloadImagesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_BEST_PICTURES);
			return f;
		case 3:
			f =  new DownloadImagesFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_RANDOM_PICTURES);
			return f;
		default:
			return null;
		}
	}

	@Override
	protected int getCountOfPages() {
		return COUNT;
	}

   
}
