package com.zuzik.ukrbashmobilefour;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DownloadImagesFragment extends ListFragment {

	protected String WEB_API_METHOD;
	protected ImagesAdapter mAdapter;
	protected ArrayList<UkrBashImage> mImages;
	private volatile boolean isDownload=false;
	
	ThumbnailDownloader<ImageView> mThumbnailThread;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mImages = new ArrayList<UkrBashImage>();
		setListAdapter(createCustomAdapter());
		setRetainInstance(true);
		setHasOptionsMenu(true);
		
		mThumbnailThread = new ThumbnailDownloader<ImageView>(new Handler());
		mThumbnailThread.setListener(new ThumbnailDownloader.Listener<ImageView>() {
			@Override
			public void onThumbnailDownloaded(ImageView token, Bitmap thumbnail) {
				if(isVisible()){
					token.setImageBitmap(thumbnail);
				}
			}
		});
		mThumbnailThread.start();
		mThumbnailThread.getLooper();
		
		startDownload();
	}
	
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mThumbnailThread.quit();
	}

	

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mThumbnailThread.clearQueue();
	}



	ListAdapter createCustomAdapter(){
		mAdapter = new ImagesAdapter(mImages);
		return mAdapter;
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		UkrBashImage img = mImages.get(position);
		Uri photoPageUri = Uri.parse(img.getImage());
		/*
		 * loading using a browser
		Intent i  = new Intent(Intent.ACTION_VIEW, photoPageUri);
		startActivity(i);
		*/
		
		Intent i = new Intent(getActivity(), PhotoPageActivity.class);
		i.setData(photoPageUri);
		startActivity(i);
		
	}
	
	void startDownload(){
		if(!isDownload){
			isDownload=true;
			new FetchItemsTask().execute();
		}
	}
	
	public String getWebApiMethod(){
		return WEB_API_METHOD;
	}
	
	public void setWebApiMethod(String webResources){
		this.WEB_API_METHOD = webResources;
	}
	
	protected class ImagesAdapter extends ArrayAdapter<UkrBashImage>{
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView==null){
				convertView = getActivity().getLayoutInflater().inflate(R.layout.ukrbash_image, parent, false);
			}
			
			UkrBashImage q = getItem(position);
			
			ImageView image = (ImageView) convertView.findViewById(R.id.image_image);
			image.setImageResource(R.drawable.img_before_load_light);
			mThumbnailThread.queueThumbnail(image, q.getThumbnail());
			
			TextView text = (TextView)convertView.findViewById(R.id.image_id);
			text.setText(q.getId());
			text = (TextView)convertView.findViewById(R.id.image_rating);
			text.setText(q.getRating());
			text = (TextView)convertView.findViewById(R.id.image_title);
			text.setText(q.getTitle());
			text = (TextView)convertView.findViewById(R.id.image_author);
			text.setText(q.getAuthor());
			text = (TextView)convertView.findViewById(R.id.image_pub_date);
			text.setText(q.getPubDate());
			
			if(position==getCount()-1){
				startDownload();
			}
			
			return convertView;
		}

		public ImagesAdapter(ArrayList<UkrBashImage> images){
			super(getActivity(), 0, images);
		}

		
	}
	
	private class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<UkrBashImage>>{
		@Override
		protected ArrayList<UkrBashImage> doInBackground(Void... params) {
			return new UkrBashFetchr().fetchImageItems(getWebApiMethod(), mImages.size());
		}
		
		@Override
		protected void onPostExecute(ArrayList<UkrBashImage> result) {
			mImages.addAll(result);
			mAdapter.notifyDataSetChanged();
			isDownload=false;
		}
		
	}
	
}
