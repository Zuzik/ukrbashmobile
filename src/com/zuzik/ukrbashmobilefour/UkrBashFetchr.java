package com.zuzik.ukrbashmobilefour;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.net.Uri;
import android.util.Log;

public class UkrBashFetchr {
	
	private static final String TAG = "UkrBashFetch";
	
	public static final String WEB_API_PUBLISHED_QUOTES = "quotes.getPublished.xml";
	public static final String WEB_API_UNPUBLISHED_QUOTES = "quotes.getUpcoming.xml";
	public static final String WEB_API_BEST_QUOTES = "quotes.getTheBest.xml";
	public static final String WEB_API_RANDOM_QUOTES = "quotes.getRandom.xml";
	
	public static final String WEB_API_PUBLISHED_PICTURES = "pictures.getPublished.xml";
	public static final String WEB_API_UNPUBLISHED_PICTURES = "pictures.getUpcoming.xml";
	public static final String WEB_API_BEST_PICTURES = "pictures.getTheBest.xml";
	public static final String WEB_API_RANDOM_PICTURES = "pictures.getRandom.xml";
	
	public static final String WEB_API_USERS = "users.getList.xml";
	
	private static final String ENDPOINT = "http://api.ukrbash.org/1/";
	private static final String API_KEY="0abdc764373c9915";
	private static final int LIMIT_QUOTES=15;
	private static final int LIMIT_IMAGES=5;
	private static final int LIMIT_USERS=30;
	private static final String PARAM_CLIENT="client";
	private static final String PARAM_START="start";
	private static final String PARAM_LIMIT="limit";
	
	private static final String XML_QUOTE="quote";
	private static final String XML_PICTURE="picture";
	private static final String XML_USER="user";
	
	byte[] getUrlBytes(String urlSpec) throws IOException{
		URL url = new URL(urlSpec);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		try{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			InputStream in = connection.getInputStream();
			if(connection.getResponseCode()!=HttpURLConnection.HTTP_OK){
				return null;
			}
			int bytesRead=0;
			byte[] buffer = new byte[1024];
				while ((bytesRead=in.read(buffer))>0) {
					out.write(buffer,0,bytesRead);
				}
				out.close();
				return out.toByteArray();
		}finally{
			connection.disconnect();
		}
	}
	
	public String getUrl(String urlSpec) throws IOException{
		return new String(getUrlBytes(urlSpec));
	}

	
	public ArrayList<UkrBashQuote> fetchQuoteItems(String webApiMethod, int startElementsNumber){
		ArrayList<UkrBashQuote> items = new ArrayList<UkrBashQuote>();
		
		try{
			String url = createQuotesUrl(webApiMethod, startElementsNumber);
			String xmlString = getUrl(url);
			
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlString));
			
			parseQuoteItems(items, parser);
			
		}catch(IOException ioe){
			Log.e(TAG,"Failed to fatch items", ioe );
		}catch(XmlPullParserException xppe){
			Log.e(TAG,"Failed to parse items", xppe );
		}

		return items;
	}
	
	public ArrayList<UkrBashImage> fetchImageItems(String webApiMethod, int startElementsNumber){
		ArrayList<UkrBashImage> items = new ArrayList<UkrBashImage>();
		
		try{
			String url = createImagesUrl(webApiMethod, startElementsNumber);
			String xmlString = getUrl(url);
			
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlString));
			
			parseImageItems(items, parser);
			
		}catch(IOException ioe){
			Log.e(TAG,"Failed to fatch items", ioe );
		}catch(XmlPullParserException xppe){
			Log.e(TAG,"Failed to parse items", xppe );
		}

		return items;
	}

	public ArrayList<UkrBashUser> fetchUserItems(String webApiMethod, int startElementsNumber){
		ArrayList<UkrBashUser> items = new ArrayList<UkrBashUser>();
		
		try{
			String url = createUsersUrl(webApiMethod, startElementsNumber);
			String xmlString = getUrl(url);
			
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlString));
			
			parseUserItems(items, parser);
			
		}catch(IOException ioe){
			Log.e(TAG,"Failed to fatch items", ioe );
		}catch(XmlPullParserException xppe){
			Log.e(TAG,"Failed to parse items", xppe );
		}

		return items;
	}
	
	//Create URL
	
	String createUrl(String webApiMethod, int startElementsNumber, int limit){
		String url = Uri.parse(ENDPOINT).buildUpon()
				.appendPath(webApiMethod)
				.appendQueryParameter(PARAM_CLIENT, API_KEY)
				.appendQueryParameter(PARAM_LIMIT, Integer.toString(limit))
				.appendQueryParameter(PARAM_START, Integer.toString(startElementsNumber))
				.build().toString();
		return url;
	}
	
	String createQuotesUrl(String webApiMethod, int startElementsNumber){
		return createUrl(webApiMethod, startElementsNumber, LIMIT_QUOTES);
	}
	
	String createImagesUrl(String webApiMethod, int startElementsNumber){
		return createUrl(webApiMethod, startElementsNumber, LIMIT_IMAGES);
	}
	
	String createUsersUrl(String webApiMethod, int startElementsNumber){
		return createUrl(webApiMethod, startElementsNumber, LIMIT_USERS);
	}
	
	//
	
	
	//Parse XML and return list of objects
	
	void parseQuoteItems(ArrayList<UkrBashQuote> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		UkrBashQuote q=null;
		int eventType = parser.next();
		while(eventType!=XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG){
				if(parser.getName().equals(XML_QUOTE)){
					q = new UkrBashQuote();
					items.add(q);
				} else if(parser.getName().equals(UkrBashQuote.XML_ID)){
					q.setId(parser.nextText());
				} else if(parser.getName().equals(UkrBashQuote.XML_AUTHOR)){
					q.setAuthor(parser.nextText());
				} else if(parser.getName().equals(UkrBashQuote.XML_PUB_DATE)){
					q.setPubDate(parser.nextText());
				} else if(parser.getName().equals(UkrBashQuote.XML_RATING)){
					q.setRating(parser.nextText());
				} else if(parser.getName().equals(UkrBashQuote.XML_TEXT)){
					q.setText(parser.nextText());
				}   
			}
			eventType = parser.next();
		}
	}
	
	void parseImageItems(ArrayList<UkrBashImage> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		UkrBashImage q=null;
		int eventType = parser.next();
		while(eventType!=XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG){
				if(parser.getName().equals(XML_PICTURE)){
					q = new UkrBashImage();
					items.add(q);
				} else if(parser.getName().equals(UkrBashImage.XML_ID)){
					q.setId(parser.nextText());
				} else if(parser.getName().equals(UkrBashImage.XML_AUTHOR)){
					q.setAuthor(parser.nextText());
				} else if(parser.getName().equals(UkrBashImage.XML_PUB_DATE)){
					q.setPubDate(parser.nextText());
				} else if(parser.getName().equals(UkrBashImage.XML_RATING)){
					q.setRating(parser.nextText());
				} else if(parser.getName().equals(UkrBashImage.XML_TITLE)){
					q.setTitle(parser.nextText());
				} else if(parser.getName().equals(UkrBashImage.XML_IMAGE)){
					q.setImage(parser.nextText());
				} else if(parser.getName().equals(UkrBashImage.XML_THUMBNAIL)){
					q.setThumbnail(parser.nextText());
				}   
			}
			eventType = parser.next();
		}
	}
	
	void parseUserItems(ArrayList<UkrBashUser> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		UkrBashUser q=null;
		int eventType = parser.next();
		while(eventType!=XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG){
				if(parser.getName().equals(XML_USER)){
					q = new UkrBashUser();
					items.add(q);
				} else if(parser.getName().equals(UkrBashUser.XML_NAME)){
					q.setName(parser.nextText());
				}   
			}
			eventType = parser.next();
		}
	}
	
	void runToText(XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.next();
		while(eventType!=XmlPullParser.TEXT){
			eventType = parser.next();
		}
	}
	
	//
}

