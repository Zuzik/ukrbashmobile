package com.zuzik.ukrbashmobilefour;

import java.util.Date;

public class UkrBashQuote {

	public static final String XML_ID = "id";
	public static final String XML_RATING = "rating";
	public static final String XML_TEXT = "text";
	public static final String XML_AUTHOR = "author";
	public static final String XML_PUB_DATE = "pub_date";
	
	private String mId;
	private String mRating;
	private String mText;
	private String mAuthor;
	private String mPubDate;
	
	public UkrBashQuote(){
		mId = "";
		mRating = "";
		mText = "";
		mAuthor = "";
		mPubDate = "";
	}
	
	public String getId() {
		return mId;
	}
	public void setId(String id) {
		if(id!=null)
			mId = id;
	}
	public String getRating() {
		return mRating;
	}
	public void setRating(String rating) {
		if(rating!=null)
			mRating = rating;
	}
	public String getText() {
		return mText;
	}
	public void setText(String text) {
		if(text!=null)
			mText = text;
	}
	public String getAuthor() {
		return mAuthor;
	}
	public void setAuthor(String author) {
		if(author!=null)
			mAuthor = author;
	}
	public String getPubDate() {
		return mPubDate;
	}
	public void setPubDate(String pubDate) {
		if(pubDate!=null){
			long timeStamp = Long.parseLong(pubDate);
			Date unixDate = new Date(timeStamp*1000);
			mPubDate = android.text.format.DateFormat.format("dd-MM-yyyy", unixDate).toString();
		}
	}
	
	
	
}
