package com.zuzik.ukrbashmobilefour;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class StartActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		
		((Button)findViewById(R.id.button_quotes)).setOnClickListener(this);
		((Button)findViewById(R.id.button_pictures)).setOnClickListener(this);
		((Button)findViewById(R.id.button_users)).setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		if(isNetworkAvailable(this)){
			switch (v.getId()) {
			case R.id.button_quotes:
				startActivity(new Intent(this, QuotesActivity.class));
				break;
			case R.id.button_pictures:
				startActivity(new Intent(this, ImagesActivity.class));
				break;
			case R.id.button_users:
				startActivity(new Intent(this, UsersActivity.class));
				break;
			default:
				break;
			}
		} else {
			Toast.makeText(this, R.string.noNetwork, Toast.LENGTH_SHORT).show();
		}
	}
	
	public static boolean isNetworkAvailable(Context context) 
	{
	    return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
	}
}
