package com.zuzik.ukrbashmobilefour;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;

public abstract class BaseSlidingActivity extends FragmentActivity {
	
	protected ViewPager viewPager;
	protected ViewPagerAdapter viewPagerAdapter;
	protected PagerTabStrip pagerTabStrip;
	
	protected abstract String getTitleByNumber(int number);
	protected abstract Fragment getFragmentByNumber(int number);
	protected abstract int getCountOfPages();
	
	 @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);
        
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);
        
        pagerTabStrip = (PagerTabStrip)findViewById(R.id.pagerTabStrip);
        //pagerTabStrip.setNonPrimaryAlpha(0.64f);
        //pagerTabStrip.setBackgroundColor(getResources().getColor(R.color.blue07));
        //pagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.orange06));
        //pagerTabStrip.setTextColor(Color.WHITE);
	 }
	 
	 private class ViewPagerAdapter extends FragmentPagerAdapter{
   	
    	public ViewPagerAdapter(FragmentManager fm){
    		super(fm);
    	}
    	
		@Override
		public Fragment getItem(int arg0) {
			return getFragmentByNumber(arg0);
		}
		
		@Override
		public int getCount() {
			return getCountOfPages();
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			return getTitleByNumber(position);
		}
    
    }
}

