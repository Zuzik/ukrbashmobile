package com.zuzik.ukrbashmobilefour;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class PhotoPageActivity extends Activity {

	private String mUrl;
	private WebView mWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_page);
		
		mUrl = getIntent().getData().toString();
		
		final ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar);
		progressBar.setMax(100);
		
		mWebView = (WebView)findViewById(R.id.webView);
		mWebView.setWebViewClient(new WebViewClient(){
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				return false;
			}
		});
		
		mWebView.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				if(newProgress==100){
					progressBar.setVisibility(View.INVISIBLE);
				} else{
					progressBar.setVisibility(View.VISIBLE);
					progressBar.setProgress(newProgress);
				}
			}
			
			@Override
			public void onReceivedTitle(WebView view, String title) {
			}
			
		});
		
		mWebView.loadUrl(mUrl);
	}
}
