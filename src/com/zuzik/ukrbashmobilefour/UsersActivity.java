package com.zuzik.ukrbashmobilefour;

import android.content.res.Resources;
import android.support.v4.app.Fragment;

public class UsersActivity extends BaseSlidingActivity {

	private final int COUNT = 1;
	
	@Override
	protected String getTitleByNumber(int number) {
		Resources res = getResources();
		switch (number) {
		case 0:
			return res.getString(R.string.users);
		default:
			return "";
		}
	}

	@Override
	protected Fragment getFragmentByNumber(int number) {
		DownloadUsersFragment f=null;
		switch (number) {
		case 0:
			f =  new DownloadUsersFragment();
			f.setWebApiMethod(UkrBashFetchr.WEB_API_USERS);
			return f;
		default:
			return null;
		}
	}

	@Override
	protected int getCountOfPages() {
		return COUNT;
	}

   
}
